
# README #

  

How to run the application locally. 

### Server setup

```
cd backend/server
pip3 install -r requirements.txt
python3 app.py
```

### Frontend setup

```
cd frontend
npm install
npm start
```
the frontend will be accessible on http://localhost:3000/
