import React, { useContext, useState } from "react";
import { Form } from "reactstrap";
import {
  Box,
  Button,
  Select,
  MenuItem,
  FormControl,
  TextField,
  InputLabel,
  Container,
  Grid,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@mui/material";
import api from "../../apiFetch/allAPIRequests";
import { makeStyles } from "@mui/styles";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../../UserContext";

const useStyles = makeStyles((theme) => ({
  gridSpacing: {
    paddingTop: "20px",
    paddingBottom: "20px",
  },
}));

function SubmissionForm(props) {
  const classes = useStyles();

  const [category, setCategory] = useState("");
  const [priority, setPriority] = useState();
  const [summary, setSummary] = useState("");
  const navigate = useNavigate();

  const {userDetails} = useContext(UserContext);

  const token = props.token;

  const handleChange = (event) => {
    event.preventDefault();
    const ticketInfo = {
      postBody: postBody,
      token: token,
    };
    api.addTicket(ticketInfo);

    navigate({ pathname: "/success" });
  };

  let handleCategoryChange = (event) => {
    setCategory(event.target.value);
  };

  let handlePriorityChange = (event) => {
    setPriority(event.target.value);
  };

  let handleSummaryChange = (event) => {
    setSummary(event.target.value);
  };

  //   getting the date for the ticket info
  const current = new Date();
  const date = `${current.getDate()}/${
    current.getMonth() + 1
  }/${current.getFullYear()}`;

  // formatted into JSON to send to server
  const postBody = {
    UserID: userDetails.username,
    Category: category,
    Priority: priority,
    Summary: summary,
    Date: date,
    Done: false,
  };

  return (
    <Container>
      <Grid>
        <Form onSubmit={handleChange}>
          <Grid item xs={12} lg={12}>
            <Box className={classes.gridSpacing}>
              <FormControl fullWidth>
                <InputLabel id="Category">Category</InputLabel>
                <Select
                  required
                  labelId="demo-simple-select-label"
                  id="Category Select"
                  value={category}
                  label="Category"
                  onChange={handleCategoryChange}
                >
                  <MenuItem key={5} value={"Forgotten Password"}>
                    Forgotten Password
                  </MenuItem>
                  <MenuItem key={1} value={"Hardware Issue"}>
                    Hardware Issue
                  </MenuItem>
                  <MenuItem key={2} value={"Access Request"}>
                    Access Request
                  </MenuItem>
                  <MenuItem key={3} value={"Report a Bug"}>
                    Report a Bug
                  </MenuItem>
                  <MenuItem key={4} value={"Other"}>
                    Other
                  </MenuItem>
                </Select>
              </FormControl>
            </Box>
          </Grid>
          <Grid item xs={12} lg={12}>
            <Box className={classes.gridSpacing}>
              <FormControl>
                <FormLabel id="priority-radio-buttons">Priority</FormLabel>
                <RadioGroup
                  required
                  row
                  name="priority-radio-buttons"
                  onChange={handlePriorityChange}
                >
                  <FormControlLabel value="1" control={<Radio />} label="1" />
                  <FormControlLabel value="2" control={<Radio />} label="2" />
                  <FormControlLabel value="3" control={<Radio />} label="3" />
                  <FormControlLabel value="4" control={<Radio />} label="4" />
                  <FormControlLabel value="5" control={<Radio />} label="5" />
                </RadioGroup>
              </FormControl>
            </Box>
          </Grid>
          <Grid item xs={12} lg={12}>
            <Box className={classes.gridSpacing}>
              <FormControl fullWidth>
                <FormLabel id="user-summary">Summary</FormLabel>
                <TextField
                  id="standard-multiline-static"
                  multiline
                  rows={4}
                  variant="standard"
                  onInput={handleSummaryChange}
                />
              </FormControl>
            </Box>
          </Grid>
          <Button type="submit" variant="contained" color="primary">
            Submit
          </Button>
        </Form>
      </Grid>
    </Container>
  );
}

export default SubmissionForm;
