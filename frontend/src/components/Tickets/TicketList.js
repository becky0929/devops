import { Grid } from "@mui/material";
import { useEffect, useState } from "react";
import api from "../../apiFetch/allAPIRequests"

import TicketCard from "./TicketCard";

// Maps all the ticket information into cards
function TicketList(props) {
    const [allData, setAllData] = useState([]);
    const [loaded, setLoaded] = useState(false);

    const userToken = props.props.token;

    useEffect(() => {
        var allTicketsJson = []
        api.getAllTickets(userToken)
        .then((result) => {
            setLoaded(true)
            for (var i = 0; i < result.length; i++) {
                allTicketsJson.push({
                    "ticketID" : result[i][0],
                    "category" : result[i][1],
                    "priority": result[i][2],
                    "summary": result[i][3],
                    "date": result[i][4],
                    "done": result[i][5]
                    })
            }
            setAllData(allTicketsJson)
        })
        .catch(error => console.log(error));
    }, [userToken])

    if (loaded) {
    return (
        <div>
            <Grid container spacing={3}>
            <Grid
                key={10}
                item
                xs={12}
                sm={12}
                md={12}
                lg={12}
            >
                <div>
                    <TicketCard allData={allData} token={userToken}/>
                </div>
            </Grid>
            </Grid>
            
        </div>
    )
} else {
    return(
        <>
        LOADING...
        </>
    )
}

};


export default TicketList;