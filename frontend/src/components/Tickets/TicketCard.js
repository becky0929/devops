import React, { useState } from "react";
import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
  Avatar,
  Box,
  Button,
  CardActions,
  Stack,
  Alert,
  Grid,
  IconButton,
  Snackbar,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import CloseIcon from "@mui/icons-material/Close";
import DoneIcon from "@mui/icons-material/Done";
import { makeStyles, withStyles } from "@mui/styles";
import api from "../../apiFetch/allAPIRequests";

const useStyles = makeStyles((theme) => ({
  card: {
    marginBottom: "1em",
  },
  avatar: {
    backgroundColor: "#454545",
    fontSize: "16pt",
    width: "50px",
    height: "50px",
  },
  cardHeader: {
    textAlign: "left",
  },
}));

const StyledTableContainer = withStyles((theme) => ({
  root: {
    maxWidth: "max-content",
    paddingLeft: "18px",
  },
}))(TableContainer);

const TableCellNoLines = withStyles({
  root: {
    borderBottom: "none",
    paddingTop: 0,
  },
})(TableCell);


// This is the content for each individual card
function TicketCard(props) {
  const classes = useStyles();
  const allTicket = props.allData;
  const [deletedOpen, setDeletedOpen] = useState(false);
  const [completedOpen, setCompletedOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setDeletedOpen(false);
    setCompletedOpen(false);
  };

  const handleDeletedClick = () => {
    setDeletedOpen(true);
  };

  const handleCompletedClick = () => {
    setCompletedOpen(true);
  };

  const action = (
    <React.Fragment>
      <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={handleClose}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
    </React.Fragment>
  );

  const markAsComplete = (ticketID) => {
    const ticketInfo = {
      ticketID: ticketID,
      token: props.token
    }
    api.markAsCompleted(ticketInfo);

    return (
      <Snackbar
        open={completedOpen}
        autoHideDuration={6000}
        onClose={handleClose}
        action={action}
      >
        <Alert onClose={handleClose} severity="success" sx={{ width: "100%" }}>
          Marked as Completed
        </Alert>
      </Snackbar>
    );
  };

  const handleDeleteItem = (ticketID) => {
    const deleteInfo = {
      ticketID: ticketID,
      token: props.token
    }
    api.deleteTicket(deleteInfo);
    window.location.reload();
  };

  return (
    <div>
      {allTicket.map((item, key) => (
        <Card className={classes.card} id={item.ticketID} key={key}>
          <CardHeader
            avatar={<Avatar className={classes.avatar}>{item.ticketID}</Avatar>}
            id={"ID"}
            title={"TITLE"}
            titleTypographyProps={{ variant: "h6" }}
            subheaderTypographyProps={{ variant: "body2" }}
            className={classes.cardHeader}
          />

          <Divider />

          <CardContent>
            <StyledTableContainer>
              <Table size="small">
                <TableBody>
                  <Grid container spacing={4}>
                    <Grid item xs="auto">
                      <TableRow key={"Category"}>
                        <TableCellNoLines align="right">
                          <b>{"Category:"}</b>
                        </TableCellNoLines>
                        <TableCellNoLines align="left">
                          <b>{item.category}</b>
                        </TableCellNoLines>
                      </TableRow>
                      <TableRow key={"User ID"}>
                        <TableCellNoLines align="right">
                          <b>{"User ID:"}</b>
                        </TableCellNoLines>
                        <TableCellNoLines align="left">
                          <b>{"user ID Space"}</b>
                        </TableCellNoLines>
                      </TableRow>
                    </Grid>
                    <Grid item xs="auto">
                      <TableRow key={"Priority"}>
                        <TableCellNoLines align="right">
                          <b>{"Priority:"}</b>
                        </TableCellNoLines>
                        <TableCellNoLines align="left">
                          <b>{item.priority}</b>
                        </TableCellNoLines>
                      </TableRow>
                      <TableRow key={"Date"}>
                        <TableCellNoLines align="right">
                          <b>{"Date Submitted:"}</b>
                        </TableCellNoLines>
                        <TableCellNoLines align="left">
                          <b>{item.date}</b>
                        </TableCellNoLines>
                      </TableRow>
                    </Grid>
                  </Grid>
                </TableBody>
              </Table>
            </StyledTableContainer>

            <Box my={1}>
              <Divider />
            </Box>
            <Box my={1}>
              <Typography variant="body1" align="left">
                <b>Summary:</b>
              </Typography>
            </Box>
            <Box my={1}>
              <Typography variant="body2" style={{ textAlign: "left" }}>
                {/* NewLineText is a lib with will delete /n if needed (TechRadarAPI.js) */}
                {item.summary}
              </Typography>
            </Box>
          </CardContent>

          <CardActions spacing={10} style={{ flexWrap: "wrap" }}>
            <span>
              <Stack direction="row" spacing={2}>
                <Button
                  variant="outlined"
                  startIcon={<DeleteIcon />}
                  onClick={() => {
                    handleDeleteItem(item.ticketID);
                    handleDeletedClick();
                  }}
                >
                  Delete
                </Button>
                <Snackbar
                  open={deletedOpen}
                  autoHideDuration={6000}
                  onClose={handleClose}
                  action={action}
                >
                  <Alert severity="error">
                    Ticket Deleted
                  </Alert>
                </Snackbar>

                <Button
                  variant="contained"
                  endIcon={<DoneIcon />}
                  disabled={item.done}
                  onClick={() => {
                    markAsComplete(item.ticketID);
                    handleCompletedClick();
                  }}
                >
                  Mark as Completed
                </Button>
                <Snackbar
                  open={completedOpen}
                  autoHideDuration={6000}
                  onClose={handleClose}
                  action={action}
                >
                  <Alert
                    onClose={handleClose}
                    severity="success"
                    sx={{ width: "100%" }}
                  >
                    Marked as Completed
                  </Alert>
                </Snackbar>
              </Stack>
            </span>
          </CardActions>
        </Card>
      ))}
    </div>
  );
}

export default TicketCard;
