import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Box,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import HomeIcon from "@mui/icons-material/Home";
import LogoutIcon from "@mui/icons-material/Logout";
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";

const useStyles = makeStyles(() => ({
  title: {
    flexGrow: 1,
  },
}));

const MenuBar = (props) => {
  const classes = useStyles();
  const navigate = useNavigate();

  function logMeOut() {
    axios({
      method: "POST",
      url: "http://127.0.0.1:5000/logout",
    })
      .then((response) => {
        props.token();
      })
      .catch((error) => {
        if (error.response) {
          console.log(error.response);
        }
        navigate({ pathname: "/login" });
      });
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar variant="dense">
          <Typography
            variant="h6"
            color="inherit"
            component="div"
            className={classes.title}
          >
            IT Support Desk
          </Typography>
          <Link to="/admin" style={{ textDecoration: "none " }} >
            <IconButton style={{ color: "white" }}>
              <AdminPanelSettingsIcon />
            </IconButton>
          </Link>

          <Link to="/home" style={{ textDecoration: "none " }}>
            <IconButton style={{ color: "white" }}>
              <HomeIcon />
            </IconButton>
          </Link>

          <IconButton style={{ color: "white" }} onClick={logMeOut}>
            <LogoutIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default MenuBar;
