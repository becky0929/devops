import {createContext} from "react";

// setting up context used in app.js
export const UserContext = createContext(null)