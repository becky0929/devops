const api = {
    getAllTickets,
    deleteTicket,
    addTicket,
    markAsCompleted
};

function getAllTickets(props) {
    const URL ="http://127.0.0.1:5000"
    const headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + props
    }
    return fetch(URL + "/tickets", {
        method: "GET",
        headers: headers,
    })
    .then((res) => res.json())
    .then((result) => {
        const data = result;
        return data; 
    })
    .catch((error) => console.log(error));
}

function deleteTicket(props) {
    const URL ="http://127.0.0.1:5000"
    const headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + props.token
    }
    return fetch(URL + "/delticket/" + props.ticketID, {
        method: "DELETE",
        headers: headers,
    })
    .then(response => response.json())
    .then(data => console.log(data))
}

function addTicket(props) {

    const URL ="http://127.0.0.1:5000"
    const headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + props.token
    }
    return fetch(URL + "/addticket", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(props.postBody)
    })
    .then(response => response.json())
}

function markAsCompleted(props) {
    const URL ="http://127.0.0.1:5000"
    const headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + props.token
    }
    return fetch(URL + "/markascompleted/" + props.ticketID, {
        method: "PUT",
        headers: headers,
        body: {status: 200}
    })
    .then(response => response.json())
    .catch((error) => console.log(error));
}


export default api;