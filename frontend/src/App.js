import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { useState } from "react";
import Login from "./pages/Login";
import CustomerPage from "./pages/CustomerPage";
import ServiceSupport from "./pages/ServiceSupportPage";
import MenuBar from "./components/MenuBar";
import SuccessPage from "./pages/SuccessPage";
import useToken from "./useToken";
import { UserContext } from "./UserContext";

function App() {
  const { token, removeToken, setToken } = useToken();
  const [userDetails, setUserDetails] = useState({
    username: "",
    admin: null,
  });

  return (
    <div className="App">
      <Router>
        <UserContext.Provider value={{ userDetails, setUserDetails }}>
          <MenuBar token={removeToken} />
          {!token && token !== "" && token !== undefined ? (
            <Login setToken={setToken} />
          ) : (
            <Routes>
              {/* <Route exact path="/login" element={<Login />} />
            <Route exact path="/" element={<Login />} /> */}
              <Route exact path="/login" element={<Login />} />
              <Route
                exact
                path="/"
                element={<CustomerPage token={token} setToken={setToken} />}
              />
              <Route
                exact
                path="/home"
                element={<CustomerPage token={token} setToken={setToken} />}
              />
              
              <Route
                exact
                path="/admin"
                element={<ServiceSupport token={token} setToken={setToken} />}
              /> 
              ) : (
               
              <Route
                exact
                path="/success"
                element={<SuccessPage token={token} setToken={setToken} />}
              />
            </Routes>
          )}
        </UserContext.Provider>
      </Router>
    </div>
  );
}

export default App;
