import SubmissionForm from "../components/TicketSubmission/SubmissionForm";

function CustomerPage(props) {

    return (
        
        <div>
            <SubmissionForm token={props.token}/>
        </div>

    )
};

export default CustomerPage;
