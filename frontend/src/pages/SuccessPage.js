import { Grid, Button } from "@mui/material";
import Alert from "@mui/material/Alert";
import AlertTitle from "@mui/material/AlertTitle";
import { useNavigate } from "react-router-dom";

function ServiceSupport(params) {
    const navigate = useNavigate()

  const handleOnClick = () => {
    navigate({ pathname: '/home' })

  };

  return (
    <div>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <Alert severity="success">
          <AlertTitle>Success</AlertTitle>
          Ticket Successfully Submitted.
        </Alert>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <Button variant="contained" onClick={() => {
                    handleOnClick();
                  }}>Make Another Report</Button>
        </Grid>
      </Grid>
    </div>
  );
}

export default ServiceSupport;
