import { Grid } from "@mui/material";
import React, { useContext } from "react";
import TicketList from "../components/Tickets/TicketList";
import { UserContext } from "../UserContext";
import { Typography } from "@mui/material";

function ServiceSupport(props) {
  const { userDetails } = useContext(UserContext);
  const admin = userDetails.admin;

  if (admin === 'True' ) {
    return (
      <div>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <TicketList props={props} />
        </Grid>
      </div>
    )
  } else {
    return (
      <div>
        <Typography>You are not Authorized to View this Page!</Typography>
      </div>
    )
  }
}

export default ServiceSupport;
