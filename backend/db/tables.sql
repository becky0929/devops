-- TO BE USED IF NOTHING IN THE DB

DROP TABLE TicketInfo;

CREATE TABLE TicketInfo( 
    Ticketid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, 
    category VARCHAR (255) NOT NULL,
    priority INTEGER NOT NULL,
    summary TEXT NOT NULL,
    date TEXT NOT NULL,
    done BOOLEAN NOT NULL,
    userid STRING REFERENCES Users (Username)
);

DROP TABLE Users;

CREATE TABLE Users (
    Userid   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    Username TEXT NOT NULL,
    Admin    BOOLEAN,
    Password TEXT
);

INSERT INTO Users (Username, Admin, Password) VALUES
    ("AdminUser", True, "pbkdf2:sha256:260000$1Brm3QeJccfnwPEa$a0e135e3dd0c7cb4a5ea367a0982d362e0051e5c5ade49377a5c6471b70c20ce")

INSERT INTO TicketInfo (category, priority, summary, date, done, userid) VALUES
    ("Forgotten Password", 1, "test", "12/12/2022", true, AdminUser), 
    ("Forgotten Password", 3, "test", "12/12/2022", false, AdminUser),
    ("Forgotten Password", 3, "test", "12/12/2022", false, AdminUser),
    ("Forgotten Password", 3, "test", "12/12/2022", false, AdminUser),
    ("Forgotten Password", 3, "test", "12/12/2022", false, AdminUser),
    ("Forgotten Password", 3, "test", "12/12/2022", false, AdminUser);

