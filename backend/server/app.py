from flask import Flask, jsonify, request
from flask_cors import CORS
import sqlite3
import json
from datetime import datetime, timedelta, timezone
from flask_jwt_extended import create_access_token,get_jwt,get_jwt_identity, unset_jwt_cookies, jwt_required, JWTManager
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)

CORS(app)

@app.route("/home")
@jwt_required()
def home_page():
    return jsonify({"status code": 200})

@app.route("/delticket/<ticketID>", methods=["DELETE"])
@jwt_required() 
def delete_ticket(ticketID):
    with sqlite3.connect("../db/database.db") as conn:
        print(ticketID)
        cursor = conn.cursor()
        result = cursor.execute("DELETE FROM TicketInfo WHERE Ticketid=?;", (ticketID,))
    return jsonify(result.fetchall())

@app.route("/tickets")
@jwt_required()
def all_ticket_info():
    with sqlite3.connect("../db/database.db") as conn:
        cursor = conn.cursor()
        result = cursor.execute("SELECT * FROM TicketInfo")
    return jsonify(result.fetchall())

@app.route("/addticket", methods=["POST"])
@jwt_required()
def add_a_ticket():
    with sqlite3.connect("../db/database.db") as conn:
        cursor = conn.cursor()
        body = request.get_json()
        category = body.get("Category")
        priority = body.get("Priority")
        summary = body.get("Summary")
        date = body.get("Date")
        done = body.get("Done")
        userid = body.get("UserID")
        result = cursor.execute("INSERT INTO TicketInfo (category, priority, summary, date, done, userid) VALUES (?, ?, ?, ?, ?, ?)", (category, priority, summary, date, done, userid))
    return jsonify(result.fetchall())


@app.route("/markascompleted/<ticketID>", methods=["PUT"]) 
@jwt_required()
def mark_as_complete(ticketID):
    with sqlite3.connect("../db/database.db") as conn:
        cursor = conn.cursor()
        result = cursor.execute("UPDATE TicketInfo SET done = true WHERE Ticketid=?;", (ticketID,))
    return jsonify(result.fetchall())


app.config["JWT_SECRET_KEY"] = "secret-key"
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(hours=1)
jwt = JWTManager(app)

@app.after_request
def refresh_expiring_jwts(response):
    try:
        exp_timestamp = get_jwt()["exp"]
        now = datetime.now(timezone.utc)
        target_timestamp = datetime.timestamp(now + timedelta(minutes=30))
        if target_timestamp > exp_timestamp:
            access_token = create_access_token(identity=get_jwt_identity())
            data = response.get_json()
            if type(data) is dict:
                data["access_token"] = access_token 
                response.data = json.dumps(data)
        return response
    except (RuntimeError, KeyError):
        # Case where there is not a valid JWT. Just return the original respone
        return response

@app.route('/token', methods=["POST"])
def create_token():
    username = request.json.get("username", None)
    password = request.json.get("password", None)
    if username != "test" or password != "test":
        return {"msg": "Wrong username or password"}, 401

    access_token = create_access_token(identity=username)
    response = {"access_token":access_token}
    return response

class UserNotFoundError(Exception):
    def __init__(self, message):
        self.message = message

class User:
    def __init__(self, username, password):
        self.username = username
        self.password = password
    
    def save_to_db(self):
        connection = sqlite3.connect('../db/database.db')
        cursor = connection.cursor()
        
        try:
            cursor.execute('INSERT INTO Users (Username, Password, Admin) VALUES (?, ?, ?)', (self.username, self.password, False))
        except:
            cursor.execute('CREATE TABLE Users (Userid   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, Username TEXT NOT NULL, Admin BOOLEAN, Password TEXT)')
            raise UserNotFoundError('The table `Users` did not exist, but it was created. Run the registration again.')
        finally:
            connection.commit()
            connection.close()
    
    @classmethod
    def find_by_username(cls, username):
        connection = sqlite3.connect('../db/database.db')
        cursor = connection.cursor()
        
        try:
            data = cursor.execute('SELECT * FROM Users WHERE Username=?', (username,)).fetchone()
            if data:
                print(data)
                jsonData = [
                    data[1],
                    data[2],
                    data[3]
                ]
                return jsonData
        finally:
            connection.close()

@app.route('/login', methods=['POST'])
def login_user():
    username = request.json.get("username")
    password = request.json.get("password")
    
    user = User.find_by_username(username)
    print(user)

    userPassword = user[2]
    userAdminRole = user[1]
    print(userAdminRole)
    
    
    if user and check_password_hash(userPassword, password):
        # return jsonify({'message': 'Password is correct'})  # You'll want to return a token that verifies the user in the future
        additional_claims = {"Admin": userAdminRole, "Username":username}
        access_token = create_access_token(identity=username, additional_claims=additional_claims)
        response = {"access_token":access_token, "username":username, "admin":userAdminRole}
        return response
    return jsonify({'error': 'User or password are incorrect'}), 401

@app.route('/register', methods=['POST'])
def register_user():
    username = request.form['username']
    password = request.form['password']
    
    try:
        User(username, generate_password_hash(password)).save_to_db()
    except Exception as e:
        return jsonify({'error': e.message}), 500
    
    return jsonify({'message': 'User registered successfully'}), 201

@app.route("/logout", methods=["POST"])
def logout():
    response = jsonify({"msg": "logout successful"})
    print(response)
    unset_jwt_cookies(response)
    return response

@app.route('/profile')
@jwt_required()
def my_profile():
    response_body = {
        "name": "Nagato",
        "about" :"Hello! I'm a full stack developer that loves python and javascript"
    }
    return response_body

if __name__ == "__main__":
    app.run()
